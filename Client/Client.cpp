/*----- PROTECTED REGION ID(Client.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        Client.cpp
//
// description : C++ source for the Client class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               Client are implemented in this file.
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <Client.h>
#include <ClientClass.h>

/*----- PROTECTED REGION END -----*/	//	Client.cpp

/**
 *  Client class description:
 *    
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//  Start         |  start
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//================================================================

namespace Client_ns
{
/*----- PROTECTED REGION ID(Client::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	Client::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : Client::Client()
 *	Description : Constructors for a Tango device
 *                implementing the classClient
 */
//--------------------------------------------------------
Client::Client(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(Client::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Client::constructor_1
}
//--------------------------------------------------------
Client::Client(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(Client::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Client::constructor_2
}
//--------------------------------------------------------
Client::Client(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(Client::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	Client::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : Client::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void Client::delete_device()
{
	DEBUG_STREAM << "Client::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(Client::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	Client::delete_device
}

//--------------------------------------------------------
/**
 *	Method      : Client::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void Client::init_device()
{
	DEBUG_STREAM << "Client::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(Client::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	Client::init_device_before
	
	//	No device property to be read from database
	
	/*----- PROTECTED REGION ID(Client::init_device) ENABLED START -----*/
	
	//	Initialize device
	
	/*----- PROTECTED REGION END -----*/	//	Client::init_device
}


//--------------------------------------------------------
/**
 *	Method      : Client::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void Client::always_executed_hook()
{
	DEBUG_STREAM << "Client::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(Client::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	Client::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : Client::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void Client::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "Client::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(Client::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	Client::read_attr_hardware
}


//--------------------------------------------------------
/**
 *	Method      : Client::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void Client::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(Client::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	Client::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command Start related method
 *	Description: 
 *
 */
//--------------------------------------------------------
void Client::start()
{
	DEBUG_STREAM << "Client::Start()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(Client::start) ENABLED START -----*/
	
	//	Add your own code
	omni_thread* subscriber = omni_thread::create(subscribe_job);
	/*----- PROTECTED REGION END -----*/	//	Client::start
}
//--------------------------------------------------------
/**
 *	Method      : Client::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void Client::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(Client::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	Client::add_dynamic_commands
}

/*----- PROTECTED REGION ID(Client::namespace_ending) ENABLED START -----*/

void Attr1Cb::push_event(Tango::EventData *) 
{
	cout << "Attr1Cb::push_event()" << endl;
}

Attr2Cb::Attr2Cb(Tango::DeviceProxy* test_device, int event_id){
	test_device = test_device;
	id = event_id;
}

void Attr2Cb::push_event(Tango::EventData *event) 
{
	cout << "Attr2Cb::push_event()" << endl;
	if (event->err)
	{
    	cout << "ERROR:" << event->errors[0].reason << endl;
	}
    else if (id != 0)
	{
		try
		{
			test_device->unsubscribe_event(id);
		}
		catch (...)
		{
			cout << "EXCEPTION" << endl;
		}
		
		id = 0;
	}
}

void subscribe_job(void*)
{
	Tango::DeviceProxy* test_device = new Tango::DeviceProxy("test/testdevice/1");
	Attr1Cb* attr1_cb = new Attr1Cb();
	int id = test_device->subscribe_event("attr1", Tango::EventType::CHANGE_EVENT, attr1_cb);
	Attr2Cb* attr2_cb = new Attr2Cb(test_device, id);
    test_device->subscribe_event("attr2", Tango::EventType::CHANGE_EVENT, attr2_cb);
}

/*----- PROTECTED REGION END -----*/	//	Client::namespace_ending
} //	namespace
