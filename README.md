# Introduction

Test to reproduce "Not able to acquire serialization monitor" in server when
unsubscribing from one attribute's events in another attribute event callback.

# Architecture

- One DS called `TestDevice` which exports in one device of class `TestDevice`
  which has two attributes: `attr1` and `attr2`.
- Another DS called `Client` which exports one device of class `Client` which
  has one command: `Start()`.


What we will to start a thread on executing `Start()` command which will:
1. Subscribe to `attr1` change events of the `TestDevice` device.
2. Subscribe to `attr2` change events of the `TestDevice` device
   with a callback which will unsubscribe from the previously subscribed
   `attr1` events.

This causes:

```
DevError[
    desc = Not able to acquire serialization (dev, class or process) monitor
  origin = TangoMonitor::get_monitor
  reason = API_CommandTimedOut
severity = ERR]
]
```

# Steps to reproduce

1. Register in Tango Database one `TestDevice` DS with instance name `test`
   with one device of `TestDevice` class, with the following names:
    `test/testdevice/1`:
    ```console
    tango_admin --add-server TestDevice/test TestDevice test/testdevice/1    
    ```
2. Register in Tango Database one `Client` DS with instance name `test`
   with one device of `Client` class, with the following names:
    `test/client/1`:
    ```console
    tango_admin --add-server Client/test Client test/client/1    
    ```
3. Start TestDevice server: 
   ```console
   python3 TestDevice.py test
   ```
4. Start Client server: 
   ```console
   python3 Client.py test
   ```
5. Execute the `Start` command

The output should look similar to:

```
╰─>$ python Client.py test -v4                                                          (sardana) 
Ready to accept request
2022-09-01 17:48:21.864574 TEST/TESTDEVICE/1 ATTR1 CHANGE [ATTR_VALID] 0.0
push_event generated the following python exception:
Traceback (most recent call last):
  File "/homelocal/zreszela/miniconda3/envs/sardana/lib/python3.9/site-packages/tango/green.py", line 210, in greener
    return executor.submit(fn, *args, **kwargs)
  File "/homelocal/zreszela/miniconda3/envs/sardana/lib/python3.9/site-packages/tango/green.py", line 92, in submit
    return fn(*args, **kwargs)
  File "/homelocal/zreszela/workspace/test-tango-unsub-server/Client.py", line 21, in attr2_cb
    self.dev.unsubscribe_event(self.id)
  File "/homelocal/zreszela/miniconda3/envs/sardana/lib/python3.9/site-packages/tango/green.py", line 195, in greener
    return executor.run(fn, args, kwargs, wait=wait, timeout=timeout)
  File "/homelocal/zreszela/miniconda3/envs/sardana/lib/python3.9/site-packages/tango/green.py", line 109, in run
    return fn(*args, **kwargs)
  File "/homelocal/zreszela/miniconda3/envs/sardana/lib/python3.9/site-packages/tango/device_proxy.py", line 1367, in __DeviceProxy__unsubscribe_event
    self.__unsubscribe_event(event_id)
PyTango.DevFailed: DevFailed[
DevError[
    desc = Not able to acquire serialization (dev, class or process) monitor
  origin = TangoMonitor::get_monitor
  reason = API_CommandTimedOut
severity = ERR]
]
^C
```

Alternatively build and run the C++ device server, so instead of step 4 do:

```console
cd Client
make
~/DeviceServers/Client test
```

Here you will get a `SIGSEGV, Segmentation fault` with the following backtrace:

```
0x000055555556cf65 in Client_ns::Attr2Cb::push_event (this=0x7fffcc0115d0, 
    event=0x7fffcc00a5c0) at Client.cpp:250
250                             test_device->unsubscribe_event(id);
(gdb) bt
#0  0x000055555556cf65 in Client_ns::Attr2Cb::push_event (this=0x7fffcc0115d0, event=0x7fffcc00a5c0) at Client.cpp:250
#1  0x00007ffff7805441 in Tango::EventConsumer::get_fire_sync_event (this=this@entry=0x7fffcc005a40, 
    device=device@entry=0x7fffcc0008c0, callback=callback@entry=0x7fffcc0115d0, ev_queue=ev_queue@entry=0x0, 
    event=event@entry=Tango::CHANGE_EVENT, event_name="idl5_change", obj_name="attr2", cb=..., 
    callback_key="tango://ct05.cells.es:10000/test/testdevice/1/attr2.idl5_change") at event.cpp:3216
#2  0x00007ffff780822f in Tango::EventConsumer::connect_event (this=this@entry=0x7fffcc005a40, 
    device=device@entry=0x7fffcc0008c0, obj_name="attr2", event=event@entry=Tango::CHANGE_EVENT, 
    callback=callback@entry=0x7fffcc0115d0, ev_queue=ev_queue@entry=0x0, filters=std::vector of length 0, capacity 0, 
    event_name="idl5_change", event_id=<optimized out>) at event.cpp:1809
---Type <return> to continue, or q <return> to quit---
#3  0x00007ffff7809544 in Tango::EventConsumer::subscribe_event (this=0x7fffcc005a40, 
    device=device@entry=0x7fffcc0008c0, attribute="attr2", event=event@entry=Tango::CHANGE_EVENT, 
    callback=callback@entry=0x7fffcc0115d0, ev_queue=ev_queue@entry=0x0, filters=std::vector of length 0, capacity 0, 
    stateless=false) at event.cpp:1250
#4  0x00007ffff7809ab0 in Tango::EventConsumer::subscribe_event (this=<optimized out>, 
    device=device@entry=0x7fffcc0008c0, attribute="attr2", event=event@entry=Tango::CHANGE_EVENT, 
    callback=callback@entry=0x7fffcc0115d0, filters=std::vector of length 0, capacity 0, stateless=false)
    at event.cpp:1129
#5  0x00007ffff771c462 in Tango::DeviceProxy::subscribe_event (this=0x7fffcc0008c0, attr_name="attr2", 
    event=Tango::CHANGE_EVENT, callback=0x7fffcc0115d0, filters=std::vector of length 0, capacity 0, 
    stateless=<optimized out>) at devapi_base.cpp:8175
#6  0x00007ffff76e19c2 in Tango::DeviceProxy::subscribe_event (this=<optimized out>, attr_name=..., 
    event=<optimized out>, callback=<optimized out>) at ./devapi.h:842
#7  0x000055555556d11e in Client_ns::subscribe_job () at Client.cpp:267
#8  0x00007ffff60a364e in omni_thread_wrapper () from /usr/lib/libomnithread.so.3
#9  0x00007ffff59fb4a4 in start_thread (arg=0x7fffea7fc700) at pthread_create.c:456
#10 0x00007ffff4ea0d0f in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:97
```