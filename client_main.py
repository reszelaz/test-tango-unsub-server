from tango import DeviceProxy, EventType
from tango.utils import EventCallback


dev = DeviceProxy("test/testdevice/1")
id_ = None
    
def attr2_cb(event):
    global id_
    if event.err:
        print("ERROR:", event.errors[0].reason)
    elif id_ is not None:
        dev.unsubscribe_event(id_)
        id_ = None

def run():
    global id_
    id_ = dev.subscribe_event(
        "attr1",
        EventType.CHANGE_EVENT,
        EventCallback()
    )
    dev.subscribe_event(
        "attr2",
        EventType.CHANGE_EVENT,
        attr2_cb
    )

if __name__ == "__main__":
    run()

