
from threading import Thread

from tango import DeviceProxy, EventType, EnsureOmniThread
from tango.utils import EventCallback


class JobThread(Thread):

    def __init__(self, dev=None):
        super(JobThread, self).__init__()
        self.dev = DeviceProxy("test/testdevice/1")
        self.id = None
    
    def attr2_cb(self, event):
        if event.err:
            print("ERROR:", event.errors[0].reason)
        elif self.id is not None:
            self.dev.unsubscribe_event(self.id)
            self.id = None

    def run(self):
        with EnsureOmniThread():
            self.id = self.dev.subscribe_event(
                "attr1",
                EventType.CHANGE_EVENT,
                EventCallback()
            )
            self.dev.subscribe_event(
                "attr2",
                EventType.CHANGE_EVENT,
                self.attr2_cb
            )

if __name__ == "__main__":
    thread = JobThread()
    thread.start()
    thread.join()
    

